
##Run mediation analysis and plot mediation model

Is the relationship between the Dependent Variable (DV; **x**) and Independent Variable (IV; **y**) mediated by **m**? 

**Load packages**

```
library(ggplot2)        # for correlation plots
library(ggcorrplot)

library(mediation)        # for mediation

library(DiagrammeR)       # for plotting mediation model
```
**Load data**

```
data <- read.csv('/Users/talithaford/Dropbox/Work/Other/bitbucket/plot_mediation/data.csv')
```

###Check correlations between variables
References: [ggcorrplot](http://www.sthda.com/english/wiki/ggcorrplot-visualization-of-a-correlation-matrix-using-ggplot2)

```
data <- read.csv('./data.csv')

#### Confirm relationships between variables ####
library(ggplot2)
library(ggcorrplot)

corr <- round(cor(data, method = "spearman"),2)
p <- round(cor_pmat(data),3)

ggcorrplot(corr, type = "lower", p.mat = p, lab = TRUE, ggtheme = ggplot2::theme_gray)

```
![alt text](./figures/corr_plot.png)

Significant correlations with **x** and **m** for **y** (rho > .3, p < .05).  


### Generate linear models
**References:**
[Introduction to mediation analysis](https://data.library.virginia.edu/introduction-to-mediation-analysis/)

```
## c path (Indirect effect of x on y)
model.0 <- lm(y ~ x, data)
summary(model.0)

## a path (x predicts m)
model.M <- lm(m ~ x, data)
summary(model.M)

## b path (m predicts y)
model.M1 <- lm(m ~ y, data)
summary(model.M1)

## c' path (Direct effect of x on y)
model.Y <- lm(y ~ x + m, data)
summary(model.Y)####Outcome variable (y)

```

**x** significantly predicts **y** (c path) and **m** (a path), and **m** predicts **y** (b path)

If the effect of **x** on **y** (c path) disappears after **m** is included in the model (c' path), so **m** fully mediates the relationship between **x** and **y**.  If the effect of **x** on **y** still exists, but in a smaller magnitude, **m** partially mediates between **x** and **y**.

### Create the mediation model

```
# Create variables with estimates and p-values
indirect = paste("c: B = ",round(model.0$coefficients[2], 2),", p = ",round(summary(model.0)$coefficients[8],3), sep = '')
direct = paste("c`: B = ",round(model.Y$coefficients[2], 2),", p = ",round(summary(model.Y)$coefficients[11],3), sep = '')
path.a = paste("B = ",round(model.M$coefficients[2], 2),", p = ",round(summary(model.M)$coefficients[8],3), sep = '')
path.b = paste("B = ",round(model.M1$coefficients[2], 2),", p = ",round(summary(model.M1)$coefficients[8],3), sep = '')
```

### create model plot
References:
[Graphviz: Dot Guide](https://graphviz.gitlab.io/_pages/pdf/dotguide.pdf ) and [Rich Iannone: DiagrammeR](https://github.com/rich-iannone/DiagrammeR)

```
code <- paste("
    digraph causal {
    # Nodes
    node [shape = reactangle]
    iv [label = 'IV (x)']
    me [label = 'Mediator (m)']
    dv [label = 'DV (y)']
    
    # Edges: lines between nodes
    edge [color = black, arrowhead = normal, style =solid]
    rankdir = LR
    iv -> me [label= '",path.a,"',fontsize=12]
    iv -> dv [label= '",indirect,"',fontsize=12] 
    me -> dv [label= '",path.b,"',fontsize=12]
    edge [color = black, arrowhead = normal, style=dotted]
    rankdir = LR
    iv -> dv [label= '",direct,"',fontsize=12] 
    
    # Graph
    #graph [overlap = true, fontsize = 10]
    }", sep = '')
```

### Plot model
```
grViz(code)
```

![mediation_model](./figures/mediation_plot.png "mediation model")

#### Test Significance of Mediation ####
**References:**
[Introduction to mediation analysis](https://data.library.virginia.edu/introduction-to-mediation-analysis/)

```
results <- mediate(model.M, model.Y, treat='x', mediator='m', boot=TRUE, sims=10000)
summary.mediate(results)
plot.mediate(results, xlab = "Estimate")
```

![mediation_significance_plot](./figures/sig_plot.png "mediation significance plot")

Average Causal Mediation Effects (AMCE; total effect minus the direct effect)  
AMCE is not significant, suggesting the mediation effect of **m** on the relationship between **x** and **y** is not significant.